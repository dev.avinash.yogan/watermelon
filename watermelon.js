const express = require('express');
const mysql = require('mysql');
const saltRounds = 10;
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const app = express();
const jwt = require('jsonwebtoken');
const fs = require('fs');
const cors = require("cors");
const unless = require('express-unless');
app.use(bodyParser.urlencoded({
    extended: true
}));

let db = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "watermelon",
    port: "3306"
});


app.use(cors());


app.use(bodyParser.json());


app.use(bodyParser.urlencoded({
    extended: true
}));

app.listen(8000, function () {
    db.connect(function (err) {
        if (err) throw err;
        console.log('Connection to Watermelon successful!');
    });
    console.log('Listening on port 8000!');
});

app.delete('/v1/users/:id', function (req, res) {

    let id = req.params.id;


    let query = `SELECT * FROM users WHERE id=${id}`;
    db.query(query, function (err, result, fields) {
        if (result.length > 0) {

            query = `DELETE FROM cards WHERE user_id=${id}`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                query = `DELETE FROM wallets WHERE user_id=${id}`;
                db.query(query, function (err, result, fields) {
                    if (err) throw err;
                    query = `DELETE FROM users WHERE id=${id}`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                        res.sendStatus(204);
                    });
                });

            });


        } else {
            res.sendStatus(403);
        }
    });
});

var checkToken = function (req, res, next) {
    console.log("\n\n\n\n\n=== Middleware ===")
    console.log("route : " + req.originalUrl + " " + req.method);
    console.log("HEADER PARAM" + JSON.stringify(req.headers));
    console.log("MY BODY:" + JSON.stringify(req.body));
    if ("x-auth-token" in req.headers) {
        //  console.log( req.headers["x-auth-token"]);
        let secretKey = fs.readFileSync('secret.key');
        //let decoded = jwt.verify(token, secretKey);
        let token = req.headers["x-auth-token"];
        //console.log(decoded);


        if (token != undefined && token != null && token != "") {
            jwt.verify(token, secretKey, (err, decoded) => {

                if (err) {
                    console.log("error");
                    res.status(401).json(JSON.stringify(token));
                } else {

                    let query = `SELECT * FROM users WHERE email = '${decoded.email}'`
                    // let query = `SELECT * FROM users WHERE apikey='${token}'`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                        if (result.length > 0) {
                            req.user = result[0];
                            next();
                        } else {

                            console.log("no users")
                            // res.status(401);
                            res.status(403).json(JSON.stringify(token));
                            //res.redirect('/v1/login');
                        }
                    });

                }
            });
        } else {

            console.log("token undefined");

            if (req.originalUrl == "/v1/wallets") {
                next();
            } else {
                res.status(401).json(JSON.stringify(token));
            }
        }

    } else {
        console.log("no field");
        if (req.originalUrl == "/v1/users") {
            console.log("enter in users");
            res.sendStatus(401);
        } else {
            next();
        }
    }
};

checkToken.unless = unless;

app.use(checkToken.unless({path: [{url: '/v1/users', methods: ['POST', 'DELETE']}, '/v1/login']}));

app.get('/', function (req, res) {
    let response = "HOMEPAGE";
    res.send(JSON.stringify(response));
});

app.post('/v1/login', function (req, res) {
    console.log("MY BODY:" + JSON.stringify(req.body));
    let email = req.body.email;
    //let id = req.body.id;
    let password = req.body.password;
    if (typeof email === 'undefined' || typeof password === 'undefined') {

        res.sendStatus(400);
    } else {
        let email = req.body.email;
        let password = req.body.password;
        let query = `SELECT * FROM users WHERE email='${email}'`; //Make a real connection
        db.query(query, function (err, result, fields) {

            if (err) throw err;
            if (result.length > 0) {
                bcrypt.compare(password, result[0].password, function (err, connected) {
                    if (err) throw err;

                    if (connected) {
                        console.log("Connected");

                        let secretKey = fs.readFileSync('secret.key');
                        let token = jwt.sign({
                            email: email
                        }, secretKey)
                        let token_object = {
                            id : result[0].id,
                            "access_token": token
                        };
                        res.send(token_object);

                        let query = `UPDATE users SET api_key = '${token}' WHERE id='${result[0].id}'`;
                        db.query(query, function (err, result, fields) {
                            console.log("Token added")
                        });

                    } else {

                        res.status(401).json({email: email, password: password});
                    }

                });
            } else {
                res.status(401).json({email: email, password: password});
            }
        });

    }
});


// ============ USERS ===============


app.post('/v1/users', function (req, res) {
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let email = req.body.email;
    let is_admin = req.body.is_admin == true ? 1 : 0;
    let api_key = "test";

    bcrypt.hash(req.body.password, saltRounds, function (err, password) {
        createUser(password).then(createWallet);
    });
    console.log("Posting user....");

    var createUser = function (password) {
        var promise = new Promise(function (resolve, reject) {
            console.log("create User");

            if (!req.body.hasOwnProperty("first_name") || !req.body.hasOwnProperty("last_name") || !req.body.hasOwnProperty("email") || !req.body.hasOwnProperty("password")
                || !req.body.hasOwnProperty("is_admin")) {
                return res.sendStatus(400);
            }
            let found = false;
            let query = `SELECT email FROM users`;
            db.query(query, function (err, result, fields) {
                let index = 0
                console.log(result.length);

                for (index; index < result.length; index++) {
                    console.log("enter");
                    if (result[index].email == email) {
                        console.log("founded");
                        found = true;
                        break
                    }
                }
                if (found) {
                    return res.sendStatus(400);
                }

                query = "INSERT INTO users (first_name, last_name, email, password, is_admin, api_key) VALUES (?,?,?,?,?,?)";
                let params = [first_name, last_name, email, password, is_admin, api_key];
                db.query(query, params, function (err, result, fields) {

                    console.log(JSON.stringify(result));
                    console.log(JSON.stringify(fields));
                    if (err) throw err;
                    res.status(200).json({
                        id: result.insertId,
                        access_token: null,
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        is_admin: is_admin == 1 ? true : false
                    });
                    resolve(result.insertId);
                });
            });
        });
        return promise;

    }

    var createWallet = function (userId) {
        var promise = new Promise(function (resolve, reject) {
            console.log("create Wallet");
            let query = "INSERT INTO wallets (user_id) VALUES (?)";
            db.query(query, userId, function (err, result, fields) {
                if (err) throw err;
                resolve(userId);
            });
        });
        return promise;
    }
});

app.get('/v1/users/', function (req, res) {

    console.log("HEADERS " + JSON.stringify(req.headers));
    // if (Object.keys(req.headers).length != 0) {
    let id = req.user.id;

    query = `SELECT id, first_name, last_name, email, is_admin FROM users WHERE id=${id}`;
    db.query(query, function (err, result, fields) {
        if (err) throw err;

        console.log("Getting users....");
        console.log("USER: " + JSON.stringify(result[0]));
       /* if (result[0].is_admin == 0) {
            console.log("sending user create ");

            result[0].is_admin = result[0].is_admin == 1 ? true : false;
            console.log(JSON.stringify(result));
            res.status(200).json(result);
        } else {*/
            query = "SELECT * FROM users"
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                res.status(200).json(result);
            });
    }

    //}
    );

});


app.get('/v1/users/:id', function (req, res) {

    console.log("HEADERS " + JSON.stringify(req.headers));

    let id = req.params.id;


    let query = `SELECT id, first_name, last_name, email, is_admin FROM users WHERE id=${id}`;
    db.query(query, function (err, result, fields) {


        if (result.length > 0) {
            console.log("Getting users....");
            console.log("USER: " + JSON.stringify(result[0]));
            // if (result[0].is_admin == 0) {
            console.log("sending user create ");

            result[0].is_admin = result[0].is_admin == 1 ? true : false;
            console.log(JSON.stringify(result[0]));
            return res.status(200).json(result[0]);

        } else {
            return res.sendStatus(404);
        }
    });

});


app.put('/v1/users/:id', function (req, res) {
    console.log("route : " + req.originalUrl);
    let id = req.params.id;
    console.log("id : " + id);
    let first_name = req.body.first_name;
    let last_name = req.body.last_name;
    let email = req.body.email;
    let is_admin = req.body.is_admin;

    let found = false;
    if (!req.body.hasOwnProperty("first_name") && !req.body.hasOwnProperty("last_name") && !req.body.hasOwnProperty("email") && !req.body.hasOwnProperty("password")
        && !req.body.hasOwnProperty("is_admin")) {
        console.log("does not exist");
        return res.sendStatus(404);
    }
    let query = `SELECT id FROM users where id = '${id}'`;
    db.query(query, function (err, result, fields) {
        let index = 0;

        for (index; index < result.length; index++) {
            console.log("enter");
            if (result[index].email == email) {
                console.log("founded");
                found = true;
                break
            }
        }

        if (result.length == 0) {
            return res.sendStatus(400);
        }
        console.log("query");
        if (result.length == 0) {
            console.log("send 404");
            return res.sendStatus(404);
        }

        if (first_name != undefined) {
            let query = `UPDATE users SET first_name = '${first_name}' WHERE id=${id}`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                console.log("first_name");
            });
        }

        if (last_name != undefined) {
            let query2 = `UPDATE users SET last_name = '${last_name}' WHERE id=${id}`;
            db.query(query2, function (err, result, fields) {
                if (err) throw err;
                console.log("last_name");

            });
        }


        if (is_admin != undefined) {
            is_admin = is_admin == 1 ? true : false;
            console.log(is_admin);
            let query5 = `UPDATE users SET  is_admin = '${is_admin}' WHERE id=${id}`;
            db.query(query5, function (err, result, fields) {
                if (err) throw err;
                console.log("is_admin");

            });
        }

        if (email != undefined) {
            if (found) {
                return res.sendStatus(400);
            } else {

                let query3 = `UPDATE users SET email = '${email}' WHERE id=${id}`;
                db.query(query3, function (err, result, fields) {
                    if (err) throw err;
                    console.log("email");

                });
            }
        }

        if (req.body.password != undefined) {
            bcrypt.hash(req.body.password, saltRounds, function (err, password) {
                //console.log("Update " + first_name + "....");
                let query4 = `UPDATE users SET password = '${password}' WHERE id=${id}`;
                db.query(query4, function (err, result, fields) {
                    if (err) throw err;
                    console.log("password");
                });
            });
        }

        console.log("finish");
        let query = `SELECT id, first_name, last_name, email, is_admin FROM users WHERE id=${id}`;
        db.query(query, function (err, result, fields) {
            if (err) throw err;
            return res.status(200).json(result[0]);
        });


    });
});


// ============ CARDS ===============


app.get('/v1/cards/', function (req, res) {

    let id = req.user.id;
    console.log(id);
    let query = `SELECT * FROM cards`;
    db.query(query, function (err, result, fields) {
        if (err) throw err;
        for (let index = 0; index < result.length; index++) {
            result[index].expired_at = new Date(result[index].expired_at).toISOString().slice(0, 10);
        }
        console.log(result);
        res.status(200).json(result);
    });

});


app.get('/v1/cards/:id', function (req, res) {
    let id = req.params.id;
    console.log(id);
    let query = `SELECT id FROM cards`;
    db.query(query, function (err, result, fields) {
        let found = false;

        let index = 0
        console.log(result.length);

        for (index; index < result.length; index++) {
            console.log("enter");
            if (result[index].id == id) {
                console.log("founded");
                found = true;
                break
            }
        }

        if (found == true) {
            query = `SELECT * FROM cards WHERE id=${id}`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                console.log(result[index].expired_at);
                result[index].expired_at = new Date(result[index].expired_at).toISOString().slice(0, 10);
                console.log(result[index]);
                res.status(200).json(result[index]);

            });
        } else {
            console.log("fuck card");
            res.sendStatus(404);
        }
    });


});


app.post('/v1/cards/', function (req, res) {
    if (Object.keys(req.body).length != 0) {
        //let user_id = req.body.id;
        let user_id = req.body.user_id;
        let last_4 = req.body.last_4;
        let brand = req.body.brand;
        let expired_at = req.body.expired_at;

        if (card_validity(brand, last_4, expired_at)) {
            console.log("Posting Cards....");
            let query = `INSERT INTO cards (user_id, last_4, brand, expired_at) VALUES ('${user_id}', '${last_4}','${brand}','${expired_at}')`;

            db.query(query, function (err, result, fields) {
                if (err) throw err;
                res.status(200).json({
                    "id": result.insertId,
                    "user_id": user_id,
                    "last_4": last_4,
                    "brand": brand,
                    "expired_at": expired_at
                });
            });
        } else {
            res.sendStatus(400);
        }
    } else {
        res.sendStatus(400);
    }
});

app.put('/v1/cards/:id', function (req, res) {

    let id = req.params.id
    let user_id = req.body.user_id;
    let last_4 = req.body.last_4;
    let brand = req.body.brand;
    let expired_at = req.body.expired_at;
    let query = `SELECT id FROM cards`;
    db.query(query, function (err, result, fields) {
        let found = false;
        let index = 0
        console.log(result.length);

        for (index; index < result.length; index++) {
            console.log("enter");
            if (result[index].id == id) {
                console.log("founded");
                found = true;
                break
            }
        }

        if (found == true) {
            console.log("Update " + id + " Card ....");


            if (card_validity(brand, last_4, expired_at)) {
                console.log("updating Cards....");
                if (user_id != undefined) {
                    let query = `UPDATE cards SET user_id = '${user_id}'  WHERE id=${id}`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                    });
                }

                if (brand != undefined) {
                    console.log(brand);
                    let query = `UPDATE cards SET brand = '${brand}'  WHERE id=${id}`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                    });
                }


                if (last_4 != undefined) {
                    console.log(last_4);
                    let query = `UPDATE cards SET last_4 = '${last_4}'  WHERE id=${id}`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                    });
                }


                if (expired_at != undefined) {
                    console.log(expired_at);
                    let query = `UPDATE cards SET expired_at = '${expired_at}'  WHERE id=${id}`;
                    db.query(query, function (err, result, fields) {
                        if (err) throw err;
                    });
                }


                query = `SELECT * FROM cards WHERE id=${id}`;
                db.query(query, function (err, result, fields) {
                    if (err) throw err;
                    console.log(result[0].expired_at);
                    result[0].expired_at = new Date(result[0].expired_at).toISOString().slice(0, 10);
                    console.log(result[0]);
                    res.status(200).json(result[0]);
                });
            } else {
                res.sendStatus(400);
            }

        } else {
            res.sendStatus(404);
        }
    });

});


app.delete('/v1/cards/:id', function (req, res) {
    let id = req.params.id;
    let found = false;
    console.log("Deleting " + id + " Card ....");
    let query = `SELECT id FROM cards`;
    db.query(query, function (err, result, fields) {
        let index = 0;
        for (index; index < result.length; index++) {

            console.log("enter");
            if (result[index].id == id) {
                console.log("founded");
                found = true;
                break
            }
        }


        if (found == true) {
            let query = `DELETE FROM cards WHERE id = ${id}`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                res.status(204).json(result);
            });
        } else {
            res.sendStatus(404);
        }
    });

});


// ============ wallets ===============


app.get('/v1/wallets/:id', function (req, res) {
    let id = req.params.id == undefined ? req.user.id : req.params.id;
    if (id != null && id != undefined) {

        console.log(id);
        let query = `SELECT * FROM wallets where id = '${id}'`;

        db.query(query, function (err, result, fields) {
            if (result.length == 0) {
                return res.sendStatus(404);
            } else {
                getUserBalance(id, function (object) {
                    return res.status(200).json(object);
                });
            }

        });

    } else {
        res.status(400).json({
            "wallet_id": null,
            "balance": 800
        })
    }


});

app.get('/v1/wallets', function (req, res) {


    if (req.headers["x-auth-token"] == "") {
        console.log("HOLA");
        res.status(200).json([{"balance": 0, "wallet_id": null}]);
    } else {
        let user_id = req.user.id;
        let query = `SELECT * FROM wallets where user_id = '${user_id}'`;
        let result_f = [];

        db.query(query, function (err, result, fields) {
                console.log(result);
                let index = 0;

                var f = function () {
                    var promise = new Promise(function (resolve, reject) {
                        console.log("my function");

                        let lenght_res = result.length;
                        Object.keys(result).forEach(function (key) {
                            console.log(result[key].id);
                            console.log(key);
                            getUserBalance(result[key].id, function (object) {
                                result_f.push(object);
                                console.log("loop");
                                index++
                                if (index == lenght_res) {
                                    console.log(lenght_res);
                                    console.log("end loop with " + JSON.stringify(result_f));
                                    resolve(result_f);
                                }
                            });
                        });

                    });
                    return promise;
                }


                f().then(function (final_obj) {
                    /*
                        console.log("HOLA");
                        res.status(200).json([{"balance": 0, "wallet_id": null}]);
                        // final_obj.push({"balance":0, "wallet_id":3});
                   */
                    console.log("resultat " + JSON.stringify(final_obj));
                    res.status(200).json(final_obj);

                })
            }
        );
    }
});


// ============ Payin ===============


app.post('/v1/payins', function (req, res) {
    console.log(req.body.wallet_id);
    console.log("Posting payin....");
    // Store hash in your password DB.
    let wallet_id = req.body.wallet_id
    // let wallet_id = req.body.wallet_id;
    let amount = req.body.amount;
    if (req.body.hasOwnProperty("wallet_id") && req.body.hasOwnProperty("amount")) {
        var numbers = /^[0-9]+$/;
        if (amount.includes(".") == false) {
            let query = `INSERT INTO payins (wallet_id, amount) VALUES ('${wallet_id}', '${amount}')`;
            // let query2 = `UPDATE wallets SET  balance = '${parseFloat(add_amount) + parseFloat(result[0].balance)}'  WHERE id = ${result[0].id}`;

            db.query(query, function (err, result, fields) {
                if (err) throw err;
                console.log("success");
                res.status(200).json(
                    {
                        "id": result.insertId,
                        "wallet_id": wallet_id,
                        "amount": parseInt(amount)
                    }
                );
            });
        } else {
            console.log("error1");
            res.sendStatus(400);
        }
    } else {
        console.log("error2");
        res.sendStatus(400);
    }


});


app.get('/v1/payins/:id', function (req, res) {

    let id = req.params.id;
    let query = `SELECT id FROM payins`;
    db.query(query, function (err, result, fields) {
        let found = false;


        let index = 0
        console.log(result.length);

        for (index; index < result.length; index++) {
            if (result[index].id == id) {
                console.log("founded");
                found = true;
                break
            }
        }

        if (found == true) {
            let query = `SELECT id, wallet_id, amount FROM payins WHERE id = '${id}'`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                console.log(result);
                res.status(200).json(result[0]);
            });
        } else {
            res.sendStatus(404);
        }
    });

});

app.get('/v1/payins', function (req, res) {
    let query = `SELECT id, wallet_id, amount FROM payins`;
    db.query(query, function (err, result, fields) {
        if (err) throw err;
        res.status(200).json(result);
    });

});


// ============ Payout ===============


app.post('/v1/payouts', function (req, res) {

    console.log("Posting payout....");
    // Store hash in your password DB.
    // let user_id = req.body.user_id;
    let wallet_id = req.body.wallet_id;

    // let wallet_id = req.body.wallet_id;
    let amount = req.body.amount;
    if (req.body.hasOwnProperty("wallet_id") && req.body.hasOwnProperty("amount")) {
        var numbers = /^[0-9]+$/;
        if (amount.includes(".") == false) {
            console.log("entered");
            let query = `SELECT id, wallet_id, amount FROM payins`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                if (parseInt(amount) <= result[0].amount) {
                    let query = `INSERT INTO payouts (wallet_id, amount) VALUES ('${wallet_id}', '${amount}')`;
                    db.query(query, function (err, result) {
                        if (err) throw err;
                        res.status(200).json({
                            "id": result.insertId,
                            "wallet_id": wallet_id,
                            "amount": parseInt(amount)
                        });
                    });
                } else {
                    res.sendStatus(400);
                }
            });


        } else {
            res.sendStatus(400);
        }
    } else {
        res.sendStatus(400);

    }


});


app.get('/v1/payouts/:id', function (req, res) {

    let id = req.params.id;
    let query = `SELECT id FROM payouts`;
    db.query(query, function (err, result, fields) {
        let found = false;
        let index = 0

        for (index; index < result.length; index++) {
            if (result[index].id == id) {
                console.log("founded");
                found = true;
                break
            }
        }

        if (found == true) {
            let query = `SELECT id, wallet_id, amount FROM payouts WHERE id = '${id}'`;
            db.query(query, function (err, result, fields) {
                if (err) throw err;
                res.status(200).json(result[0]);
            });
        } else {
            res.sendStatus(404);
        }
    });

});


app.get('/v1/payouts/', function (req, res) {

    //  let user_id = req.user.id;
    let query = `SELECT id, wallet_id, amount FROM payouts`;
    db.query(query, function (err, result, fields) {
        if (err) throw err;
        res.status(200).json(result);
    });

});


// ============ Transfert ===============

app.post('/v1/transfers', function (req, res) {


    if (req.headers["x-auth-token"] == "") {
        return res.sendStatus(401);
    }
    let debitedId = req.user.id;
    let credited_wallet_id = req.body.credited_wallet_id;
    let take_amount = req.body.amount;
    let query = `SELECT * FROM wallets where id = '${credited_wallet_id}'`;
    let found = false;

    if (take_amount.includes(".")) {
        return res.sendStatus(400);
    }


    db.query(query, function (err, result, fields) {
        let found = false;
        if (result.length > 0) {
            if (result[0].user_id == req.user.id) {
                return res.sendStatus(400);
            } else {
                found = true;
            }
        } else {
            if (credited_wallet_id != "") {
                return res.sendStatus(400);
            }
        }


        let debited_wallet_id = null;

        let query = `SELECT * FROM wallets WHERE user_id = '${debitedId}'`;


        db.query(query, function (err, result, fields) {
            if (result.length > 0) {
                console.log(result);
                debited_wallet_id = result[0].id;
            }

            getUserBalance(debited_wallet_id, function (object) {
                console.log(object.balance + " vs " + take_amount);
                if (object.balance < take_amount) {
                    return res.sendStatus(400);
                }
                if (credited_wallet_id == "") {

                    console.log("credited_id null");
                    query = `INSERT INTO transfers (debited_wallet_id ,credited_wallet_id, amount) VALUES ('${debited_wallet_id}', null,'${take_amount}')`;
                    db.query(query, function (err, result) {
                        return res.status(200).json({
                            "id": result.insertId,
                            "wallet_id": null,
                            "amount": parseInt(take_amount),
                        });

                    });

                } else {

                    query = `INSERT INTO transfers (debited_wallet_id ,credited_wallet_id, amount) VALUES ('${debited_wallet_id}', '${credited_wallet_id}','${take_amount}')`;
                    db.query(query, function (err, result) {
                        if (err) throw err;
                         console.log("cool !")
                        console.log(JSON.stringify(result));
                        console.log(JSON.stringify({
                            "id": result.insertId,
                            "wallet_id": credited_wallet_id,
                            "amount": take_amount,
                        }));
                        return res.status(200).json({
                            "id": result.insertId,
                            "wallet_id": credited_wallet_id,
                            "amount": take_amount,
                        })

                    });
                }

            });
        });


    });
});


app.get('/v1/transfers/:id', function (req, res) {


    if (req.headers["x-auth-token"] == "") {
        res.sendStatus(401);
    } else {
        console.log("start get one transfert");
        let id = req.params.id;
        console.log(id);
        let query = `SELECT * FROM transfers`;
        db.query(query, function (err, result, fields) {
            console.log(JSON.stringify(result));
            if (result.length > 0) {

                Object.keys(result).forEach(function (key) {
                    let row = result[key];
                    if (row.id == id) {
                        return res.status(200).json(result[0]);
                    }
                });
                console.log("senddd");

            } else {
                res.sendStatus(404);
            }
        });

    }
});


app.get('/v1/transfers/', function (req, res) {


    if (req.headers["x-auth-token"] == "") {
        res.sendStatus(401);
    } else {


        console.log("start get transfert");

        let query = `SELECT * FROM transfers`;
        db.query(query, function (err, result, fields) {
            console.log(JSON.stringify(result));
            if (result.length > 0) {
                console.log("senddd");
                return res.status(200).json(result);
            } else {
                res.sendStatus(404);
            }
        });


    }
});


//========================== BUNCH OF FUNCTION =============================


function getUserBalance(wallet_id, callback) {
    console.log("getuserbalance");
    let payin = 0;
    let payout = 0;
    let credited = 0;
    let debited = 0;

    console.log("wallet id " + wallet_id);
    if (wallet_id != "" && wallet_id != undefined && wallet_id != null && wallet_id != "$0") {
        //Get payins amount
        let query = `SELECT amount FROM payins WHERE wallet_id = ${wallet_id}`;
        //Get payouts amount
        let query2 = `SELECT amount FROM payouts WHERE wallet_id = ${wallet_id}`;
        let query3 = `SELECT amount FROM transfers WHERE credited_wallet_id = ${wallet_id}`;
        let query4 = `SELECT amount FROM transfers WHERE debited_wallet_id = ${wallet_id}`;

        db.query(query, function (err, result) {
            console.log("getuserbalance");
            Object.keys(result).forEach(function (key) {
                let row = result[key];
                console.log("amount payins:" + row.amount);
                payin = payin + row.amount;
            });
            if (err) throw err;
        });

        db.query(query2, function (err, result) {
            console.log("getuserbalance1");
            Object.keys(result).forEach(function (key) {
                let row = result[key];
                console.log("amount payouts :" + row.amount);
                payout = payout + row.amount;
            });
            if (err) throw err;

        });
        db.query(query3, function (err, result) {
            console.log("getuserbalance2");
            Object.keys(result).forEach(function (key) {
                let row = result[key];
                console.log("amount credited:" + row.amount);
                credited = credited + row.amount;
            });
            if (err) throw err;

        });
        db.query(query4, function (err, result) {
            console.log("getuserbalance3");
            Object.keys(result).forEach(function (key) {
                let row = result[key];
                console.log("amount debited:" + row.amount);
                debited = debited + row.amount;
            });
            if (err) throw err;

            console.log("payin : " + payin);
            console.log("payout : " + payout);
            console.log("credited : " + credited);
            console.log("payout : " + debited);
            callback({
                "wallet_id": wallet_id,
                "balance": (payin + credited) - (payout + debited)
            });


        });
    } else {
        console.log("payin : " + payin);
        console.log("payout : " + payout);
        console.log("credited : " + credited);
        console.log("payout : " + debited);
        callback({
            "balance": (payin + credited) - (payout + debited),
            "wallet_id": wallet_id
        });
    }


}


function card_validity(brand, last_4, expired_at) {
    let validity = false;
    let validity2 = false;
    let validity3 = false;

    let brands = ["visa", "master_card", "american_express", "union_pay", "jcb"];

    validity = brands.includes(brand);

    validity2 = last_4.length == 4 ? true : false;
    validity3 = new Date(expired_at) > Date.now() ? true : false;

    console.log("brand : " + brand);
    validity = brand == undefined ? true : validity;
    validity2 = last_4 == undefined ? true : validity2;
    validity3 = expired_at == undefined ? true : validity3;

    console.log(validity + " " + validity2 + " " + validity3);
    if (validity == false || validity2 == false || validity3 == false) {
        return false
    } else {
        return true;
    }
}